import java.io.*;
import java.util.Arrays;

class Processor{

  private static pLinkedList transaction = new pLinkedList();
  public static void main(String[] args){
    Processor Processor = new Processor();

    if(args.length != 1)
      Processor.printErr(0, Integer.MIN_VALUE);
    else
    Processor.readFile(args[0]);
  }

  /** Read the file in and perform error checking
   * @param fileName Name of the file containing transactions
   */
  public void readFile(String fileName){
    try {
      BufferedReader br = new BufferedReader(new FileReader(fileName));
      int lineNumber = 0;
      String inputLine;
      String[] transactionDetails;

      while ((inputLine = br.readLine()) != null) {
        transactionDetails = inputLine.split(" ");
        if (transactionDetails.length != 3) { // Must have 3 arguments
          printErr(1, lineNumber);
        } else if (!(transactionDetails[0].equals("D") || transactionDetails[0].equals("W"))) { // Must have D or W (Deposit/Withdrawl)
          printErr(2, lineNumber);
        } else if (transactionDetails[1].length() != 7 || !transactionDetails[1].substring(0, 1).equals("8")) { // Account number, must be 8XXXXXX
          printErr(3, lineNumber);
        } else if(Float.parseFloat(transactionDetails[2]) < 0 ){
          printErr(4, lineNumber);
        } else {
          processTransaction(transactionDetails);
          transaction.printLog("transactions.log");
          //transaction.deleteNode(8501204);
          transaction.printLog("deleted.log");
        }
        lineNumber++; // line number of error

      }
      br.close();
    } catch (FileNotFoundException e) {
      printErr(0, Integer.MIN_VALUE);
    } catch (IOException e) {
      printErr(0, Integer.MIN_VALUE);
    }
  }

  private void processTransaction(String[] t) {
    String transactionType = t[0];
    int accountNum = Integer.parseInt(t[1]);
    float amount = Float.parseFloat(t[2]);

    if(transactionType.equals("D")){
      transaction.Deposit(accountNum, amount);
    }
    else if(transactionType.equals("W")){
      transaction.Withdrawl(accountNum, amount);
    }
  }

  /** Handles printing of errors
   * @param n Error number
   * @param l Line error occured
   */
  public void printErr(int n, int l){
    final String err = "ERROR: Line ", format = "Expected Format: ";
    switch (n){
      case 0:
          System.out.printf("%s File not found \n %s java XactProcess <filename>\n", err, format);
          break;
      case 1:
          System.out.printf("%s%d Insufficient parameters. \n %s [TRANSACTION TYPE] [ACCOUNT NUMBER] [SUM]\n", err, l, format);
          break;
      case 2:
          System.out.printf("%s%d Invalid Transaction Type. \n %s Transactions must be 'D' or 'W'\n", err, l, format);
          break;
      case 3:
          System.out.printf("%s%d Invalid Account Number. \n %s Account Numbers must be format 8XXXXXX\n", err, l, format);
          break;
      case 4:
        System.out.printf("%s%d Invalid  Transaction Amount. \n %s Transaction amount must be positive decimal value\n", err, l, format);
        break;
    }
  }
}
