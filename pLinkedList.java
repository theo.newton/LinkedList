import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Theodore Newton on 5/26/2017.
 */
public class pLinkedList {
    private Node head;

    /**
     *  Node to hold account numbers and balances
     */
    private class Node{
        public Node next;
        public int storedAccountNumber;
        public float storedAccountBalance;

        public Node(int sAN, float sAB){
            storedAccountNumber = sAN;
            storedAccountBalance = sAB;
        }
    }

    public void insert(int k, float v){

        if(head == null){
            head = new Node(k, v);
            return;
        }
        // Keeping in ASC order
        if(k <  head.storedAccountNumber){
            Node tempNode = new Node(k,v);
            tempNode.next = head;
            head=tempNode;
            return;
        }

        // Go through list to find it's place
        Node tempNode = head; // Set start of list
        while (tempNode.next != null && tempNode.next.storedAccountNumber < k) {
            tempNode = tempNode.next; //find place in list
        }
        Node newAccount = new Node(k,v);
        newAccount.next = tempNode.next;
        tempNode.next = newAccount;
    }

    /**
     * Delete the specified account
     * @param account account to be deleted
     * @return
     */
    public boolean deleteNode(int account) {
        if (head == null || head.next == null) {
            return false; // Failure
        }
        Node tempNode = head;
        // Find the node to be deleted
        while(tempNode.next != null && tempNode.next.storedAccountNumber < account){
            tempNode = tempNode.next;
        }
        // Now delete it
        if (tempNode.storedAccountNumber == account) {
            tempNode.storedAccountNumber = tempNode.next.storedAccountNumber;
            tempNode.next = tempNode.next.next;
        }
        return true;
    }

    public void Deposit(int k, float v) {
        //Find the account
        Node tempNode = head;
        while(tempNode != null && tempNode.storedAccountNumber < k){
            tempNode = tempNode.next;
        }
        if(tempNode != null && tempNode.storedAccountNumber == k) {
            tempNode.storedAccountBalance += v;
        }
        else{
            insert(k,v);
        }
    }

    public void Withdrawl(int k, float v) {
        Deposit(k, 0-v);
    }

    public void printLog(String fn)throws IOException {
        String output = fn;
        BufferedWriter bw = new BufferedWriter(new FileWriter(output));
        Node tempNode = head;

        while(tempNode != null){
            String outputLine = tempNode.storedAccountNumber + String.format(":\t$%.2f", tempNode.storedAccountBalance);
            bw.write(outputLine + "\n");
            tempNode = tempNode.next;
        }
        bw.close();

    }

}
